/********************************************************************************
** Form generated from reading UI file 'arcanoid.ui'
**
** Created by: Qt User Interface Compiler version 5.0.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARCANOID_H
#define UI_ARCANOID_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_arcanoid
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QGraphicsView *graphicsView;
    QPushButton *pushButton;
    QLabel *label;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *arcanoid)
    {
        if (arcanoid->objectName().isEmpty())
            arcanoid->setObjectName(QStringLiteral("arcanoid"));
        arcanoid->resize(1054, 665);
        arcanoid->setMouseTracking(true);
        centralWidget = new QWidget(arcanoid);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        graphicsView->viewport()->setProperty("cursor", QVariant(QCursor(Qt::ArrowCursor)));
        graphicsView->setMouseTracking(true);
        graphicsView->setDragMode(QGraphicsView::NoDrag);
        graphicsView->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
        graphicsView->setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing);

        gridLayout->addWidget(graphicsView, 2, 0, 1, 3);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMaximumSize(QSize(120, 16777215));
        pushButton->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(pushButton, 0, 0, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 1, 1, 1);

        arcanoid->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(arcanoid);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1054, 21));
        arcanoid->setMenuBar(menuBar);
        statusBar = new QStatusBar(arcanoid);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        arcanoid->setStatusBar(statusBar);

        retranslateUi(arcanoid);

        QMetaObject::connectSlotsByName(arcanoid);
    } // setupUi

    void retranslateUi(QMainWindow *arcanoid)
    {
        arcanoid->setWindowTitle(QApplication::translate("arcanoid", "arcanoid", 0));
        pushButton->setText(QApplication::translate("arcanoid", "New Game", 0));
        label->setText(QApplication::translate("arcanoid", "Health: 4", 0));
    } // retranslateUi

};

namespace Ui {
    class arcanoid: public Ui_arcanoid {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARCANOID_H
