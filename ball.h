#ifndef BALL_H
#define BALL_H
#include "QGraphicsEllipseItem"
//#include<QGraphicsPixmapItem>


class Ball : public QGraphicsPixmapItem
{
public:
    Ball(QPixmap &pixmap, QGraphicsItem *parent);
    float angle;
    short verticalDirection;

};

#endif // BALL_H
