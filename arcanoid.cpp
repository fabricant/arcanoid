#include "arcanoid.h"
#include "ui_arcanoid.h"
#include "scene.h"
#include <QOpenGLWidget>
//#include <QDebug>
arcanoid::arcanoid(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::arcanoid)
{
    ui->setupUi(this);
    static scene sc;
    connect(ui->pushButton,SIGNAL(clicked()),&sc,SLOT(newGame()));
    ui->graphicsView->setScene(&sc);
    ui->graphicsView->setViewport(new QOpenGLWidget);
    connect(&sc, SIGNAL(healthSignal(QString)), ui->label, SLOT(setText(QString)));

}

void arcanoid::resizeEvent(QResizeEvent *){
    ui->graphicsView->fitInView(ui->graphicsView->sceneRect(), Qt::KeepAspectRatio);
}



arcanoid::~arcanoid()
{
    delete ui;
}

