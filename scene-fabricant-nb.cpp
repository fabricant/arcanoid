#include "scene.h"
#include <QDebug>
#include <QKeyEvent>
#include <math.h>
#include<QCursor>
//#include <process.h>
#include <QGraphicsView>
#include <QKeyEvent>
#include<QTime>
//#include <time.h>

scene::scene(QObject *parent) :
    QGraphicsScene(parent)
{
    newGame();
    connect(&timer, SIGNAL(timeout()), this, SLOT(updateScene()));
    setBackgroundBrush(QBrush(Qt::white,QPixmap("stars.jpg")));
}



void scene::newGame(){
    QPen lpen, rpen;
    lpen.setBrush(QBrush(QPixmap("left.jpg")));
    lpen.setWidth(20);
    rpen.setBrush(QBrush(QPixmap("right.jpg")));
    rpen.setWidth(20);
    this->clear();
    ballList.clear();
    listBonus.clear();
    round.clear();
    mapBonus.clear();
    racket = addPixmap(QPixmap("racket.png"));
    racket->setY(700);
    racket->setData(0, "racket");
    addBall(0, 679, -2, 0, false);
    addLine(0,   0,   800, 0);
    addLine(-10,   0,   -10,   800)->setPen(lpen);
    addLine(810, 0,   810, 800)->setPen(rpen);
    addLine(0,   800, 800, 800)->setData(0, "zero");
    timer.start(10);

    brickCount = level1();
    level      = 1;
    health     = 4;
    healthSignal(QString("Health: 4"));
}


//��������� � ��������
void scene::updateScene(){
    static float rx = 0;

    racketMove(rx);
    foreach(Ball* ball, ballList)
    {
        if (ball->data(0)==true)
        {
            ballMoveX(ball);
            ballMoveY(rx, ball);

        }
        else
        {
            ball->setX(racket->x()+racket->boundingRect().width()/2-10);
            ball->angle = getAngle(rx);// � ����������� �� �������� �������� ������� �������� ���������� �� ������� ������������ ��� �� ��� � ��� ������ ����
        }
    }

    rx = racket->x();
    foreach(QGraphicsPixmapItem* brick, round){
        brick->setRotation(brick->rotation()+1);
    }

    foreach(QGraphicsPixmapItem *item, listBonus){
        item->setY(item->y()+3);

        if(item->collidesWithItem(racket))
        {
            listBonus.removeOne(item);
            (this->*mapBonus.value(item))();// ���������� ��������� �� ������� � ��������� �
            removeItem(item);
            item=0;
        }
        else
            if(item->y()>=780)
            {
                listBonus.removeOne(item);
                removeItem(item);
                delete item;
                item=0;
            }
    }

    if(brickCount == 0)
    {
        switch (level)
        {
        case 1: brickCount = level2();
                break;
        case 2: brickCount = level3();
                break;
        case 3: QGraphicsPixmapItem *winner=addPixmap(QPixmap("winner.jpg"));
                winner->setZValue(1000);
                winner->setX(200);
                timer.stop();
                break;
        }
        level++;


        Ball *ball=ballList.first();
        ballList.removeOne(ball);
        ball->setY(679);
        ball->setData(0,false);
        normalRacket();
        foreach(Ball* ball,ballList)
        {
            ballList.removeOne(ball);
            removeItem(ball);
            delete ball;
            ball=0;
        }
        ballList<<ball;

    }
}

void scene::racketMove(float &rx){
    racket->setX(this->views().first()->mapToScene(this->views().first()->mapFromGlobal(QCursor::pos())).x()-racket->boundingRect().width()/2);
    if (racket->x()<0)
        racket->setX(0);
    if (racket->x()>800-racket->boundingRect().width())
        racket->setX(800-racket->boundingRect().width());

    foreach(Ball* ball, ballList){
        if(racket->collidesWithItem(ball))
        {
        float angle = getAngle(rx);
            ball->angle += angle;
            if(angle>0)
            {
                ball->setX(racket->x()+racket->boundingRect().width());
                if(ball->x()>=780)
                {// ���� ������� ������� ��� ���� ��������. ���� ����� ���� ���� ��� ����������� ��� �������������
                    float chord = 0;
                    if(ball->y()+10 < racket->y())
                    {
                        chord = racket->y()-(ball->y()+10);
                        ball->setX(779 - chord);
                        racket->setX(ball->x() + chord - racket->boundingRect().width());
                        ball->angle=angle/5;
                        ball->verticalDirection = -2;
                        ball->setY(ball->y()+ball->verticalDirection);
                    }
                    else
                        if(ball->y() > racket->y()+10)
                        {
                            chord = ball->y() + 10 - (racket->y()+racket->boundingRect().height());
                            ball->setX(779 - chord);
                            racket->setX(ball->x() + chord - racket->boundingRect().width());
                            ball->angle=angle/5;
                            ball->verticalDirection = 2;
                            ball->setY(ball->y()+ball->verticalDirection);

                        }
                        else
                        {
                            ball->setX(779);
                            racket->setX(ball->x()-racket->boundingRect().width());
                            ball->angle=0;
                            ball->verticalDirection=2;

                        }
                }
                if (ball->angle>5)
                    ball->angle = 5;
            }
            else
            {
                ball->setX(racket->x()-ball->boundingRect().width());
                if(ball->x()<=0)// ���� ������� ������� ��� ���� ��������. ���� ����� ���� ���� ��� ����������� ��� �������������
                {
                    float chord = 0;
                    if(ball->y()+10 < racket->y())
                    {
                        chord = racket->y()-(ball->y()+10);
                        ball->setX(1);
                        racket->setX(21 - chord);
                        ball->angle=angle/5;
                        ball->verticalDirection = -2;
                        ball->setY(ball->y()+ball->verticalDirection);
                    }
                    else
                        if(ball->y() > racket->y()+10)
                        {
                            chord = ball->y() + 10 - (racket->y()+racket->boundingRect().height());
                            ball->setX(1);
                            racket->setX(21 - chord);
                            ball->angle=angle/5;
                            ball->verticalDirection = 2;
                            ball->setY(ball->y()+ball->verticalDirection);

                        }
                        else
                        {
                            racket->setX(21);
                            ball->angle = 0;
                            ball->setX(1);
                            ball->verticalDirection=-2;
                        }
                }
                if (ball->angle<-5)
                    ball->angle = -5;
            }
        }
    }
}

void scene::ballMoveX(Ball *ball){
    QList<QGraphicsItem *> items;
    ball->setX(ball->x()+ball->angle);

    items  = ball->collidingItems();

    foreach (QGraphicsItem *item, items)
    {
        if(item->data(1).toString() != "ball")
        {
            ball->angle=-ball->angle;
            ball->setX(ball->x()+ball->angle);
            if (item->data(0)=="brick")
            {
                round.removeOne((QGraphicsPixmapItem*)item);
                removeItem(item);
                delete item;
                item=0;
                --brickCount;
                return;
            }
            if(item->data(0).toString()=="bonus")
            {
                listBonus<<(QGraphicsPixmapItem*)item;
                item->setData(0,0);
            }
        }
    }


}

void scene::ballMoveY(float &rx, Ball *ball){
    static QList<QGraphicsItem *> items;
    ball->setY(ball->y()+ball->verticalDirection);
    items  = ball->collidingItems();
    foreach (QGraphicsItem *item, items)
    {

        if(item->data(1).toString() != "ball")
        {
            ball->verticalDirection=-ball->verticalDirection;
            ball->setY(ball->y()+ball->verticalDirection);

            if (item->data(0)=="brick"){
                round.removeOne((QGraphicsPixmapItem*)item);
                removeItem(item);
                delete item;
                item=0;
                --brickCount;
                return;
            }
            else
                if (item == racket)
                {
                    getAngle(rx, ball->angle);//���� ��� ���������� � �������� ���� �������� �������� � ����������� �� �������� � ����������� �������
                }
                else
                    if(item->data(0) == "zero")
                    {

                        if(ballList.size()==1)
                        {

                            health--;
                            healthSignal(QString("Health: "+QString::number(health)));
                            if(health == 0)
                            {
                                QGraphicsPixmapItem *haha=addPixmap(QPixmap("nelson-muntz.png"));
                                haha->setZValue(1000);
                                haha->setX(200);
                                ballList.removeOne(ball);
                                removeItem(ball);
                                delete ball;
                                ball = 0;
//                                timer.stop();
                            }
                            else
                            {
                                ball->setData(0, false);
                                ball->setY(679);
                                ball->angle = 0;
                            }
                        }
                        else
                        {
                            ballList.removeOne(ball);
                            removeItem(ball);
                            delete ball;
                            ball = 0;
                        }
                    }
                    else
                        if(item->data(0).toString()=="bonus")
                        {
                            listBonus<<(QGraphicsPixmapItem*)item;
                            item->setData(0,0);
                        }

        }
    }

}


float scene::getAngle(float &rx){
    static float angle = 0;
    angle = (racket->x()-rx)/5;

    if (angle > 5)
        angle = 5;
    else
        if(angle <-5)
            angle = -5;

    return angle;
}

void scene::getAngle(float &rx, float &angle){
    angle += (racket->x()-rx)/5;

    if (angle > 5)
        angle = 5;
    else
        if(angle <-5)
            angle = -5;

}

void scene::keyPressEvent(QKeyEvent *event){

        if(event->key() == Qt::Key_Space)
            ballList.first()->setData(0,true);

}

void scene::mousePressEvent(QGraphicsSceneMouseEvent *event){
    manyBalls();
    manyBalls();
    manyBalls();
    manyBalls();
    manyBalls();
}


//������
void scene::bigRacket(){
    if(racket->x()+200>800)
        racket->setX(600);
//    racket->setRect(0,0,200,20);
    racket->setPixmap(QPixmap("bigracket.jpg"));
    QTimer::singleShot(15000,this,SLOT(normalRacket()));

}

void scene::normalRacket(){
    racket->setPixmap(QPixmap("racket.jpg"));
}

void scene::manyBalls(){
    addBall(390, 670, -2, -5);
    addBall(390, 670, -2, -2);
    addBall(390, 670, -2,  0);
    addBall(390, 670, -2,  2);
    addBall(390, 670, -2,  5);
}

void scene::healthPlus(){
    health++;
    healthSignal(QString("Health: "+QString::number(health)));
}


QGraphicsPixmapItem* scene::addBrick(int x, int y){
    QGraphicsPixmapItem *brick;
    brick=addPixmap(QPixmap("brick.png"));
    brick->setOffset(x,y);
    brick->setData(0, "brick");
    return brick;
}

Ball* scene::addBall(int x, int y, short verticalDirection, float angle, bool move){
    static QPixmap pixmap   = QPixmap("ball.png");
    Ball *ball              = new Ball(pixmap,0,this);
    ball->angle             = angle;
    ball->verticalDirection = verticalDirection;
    ball->setPos(x,y);
    ball->setData(0,move);
    ball->setData(1,"ball");
    ballList<<ball;
    return ball;
}


//������
int scene::level1(){

    QGraphicsPixmapItem *bon;
    void (scene::*pBonusFunction)();

    bon = addPixmap(QPixmap("br.png"));
    bon->setPos(720,20);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::bigRacket;
    mapBonus.insert(bon, pBonusFunction);

    bon = addPixmap(QPixmap("mb.png"));
    bon->setPos(360, 40);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::manyBalls;
    mapBonus.insert(bon, pBonusFunction);

    bon = addPixmap(QPixmap("+1.png"));
    bon->setPos(520, 0);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::healthPlus;
    mapBonus.insert(bon, pBonusFunction);

    addBrick(0,   0);
    addBrick(40,  0);
    addBrick(80,  0);
    addBrick(120, 0);
    addBrick(160, 0);
    addBrick(200, 0);
    addBrick(240, 0);
    addBrick(280, 0);
    addBrick(320, 0);
    addBrick(360, 0);
    addBrick(400, 0);
    addBrick(440, 0);
    addBrick(480, 0);
    addBrick(520, 0);
    addBrick(560, 0);
    addBrick(600, 0);
    addBrick(640, 0);
    addBrick(680, 0);
    addBrick(720, 0);
    addBrick(760, 0);

    addBrick(40,  20);
    addBrick(80,  20);
    addBrick(120, 20);
    addBrick(160, 20);
    addBrick(200, 20);
    addBrick(240, 20);
    addBrick(280, 20);
    addBrick(320, 20);
    addBrick(360, 20);
    addBrick(400, 20);
    addBrick(440, 20);
    addBrick(480, 20);
    addBrick(520, 20);
    addBrick(560, 20);
    addBrick(600, 20);
    addBrick(640, 20);
    addBrick(680, 20);
    addBrick(720, 20);

    addBrick(80,  40);
    addBrick(120, 40);
    addBrick(160, 40);
    addBrick(200, 40);
    addBrick(240, 40);
    addBrick(280, 40);
    addBrick(320, 40);
    addBrick(360, 40);
    addBrick(400, 40);
    addBrick(440, 40);
    addBrick(480, 40);
    addBrick(520, 40);
    addBrick(560, 40);
    addBrick(600, 40);
    addBrick(640, 40);
    addBrick(680, 40);

    for(int i=0; i<8; i++)
    {
        static int x=20, y=10;
        addBrick(80,  60+x*i);
        addBrick(120, 60+x*i);
        addBrick(160, 60+x*i);
        addBrick(600, 60+x*i);
        addBrick(640, 60+x*i);
        addBrick(680, 60+x*i);

        addBrick(90+y*i,  220+x*i);
        addBrick(130+y*i, 220+x*i);
        addBrick(170+y*i, 220+x*i);
        addBrick(590-y*i, 220+x*i);
        addBrick(630-y*i, 220+x*i);
        addBrick(670-y*i, 220+x*i);
    }






    for (float rad=0; rad<6; rad+=M_PI_4)
    {
        QGraphicsPixmapItem *brick;
        brick=addBrick(-20,100);
        brick->setPos(400,200);
        brick->setRotation(rad * 57.295779513);
        brick->setData(0, "brick");
        round<<brick;
    }

    return 158;

}

int scene::level2(){
    QGraphicsPixmapItem *bon;
    void (scene::*pBonusFunction)();

    bon = addPixmap(QPixmap("br.png"));
    bon->setPos(200,260);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::bigRacket;
    mapBonus.insert(bon, pBonusFunction);

    bon = addPixmap(QPixmap("mb.png"));
    bon->setPos(680, 20);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::manyBalls;
    mapBonus.insert(bon, pBonusFunction);

    bon = addPixmap(QPixmap("+1.png"));
    bon->setPos(280, 100);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::healthPlus;
    mapBonus.insert(bon, pBonusFunction);

    addBrick(0,   0);
    addBrick(40,  20);
    addBrick(80,  0);
    addBrick(120, 20);
    addBrick(160, 0);
    addBrick(200, 20);
    addBrick(240, 0);
    addBrick(280, 20);
    addBrick(320, 0);
    addBrick(360, 20);
    addBrick(400, 0);
    addBrick(440, 20);
    addBrick(480, 0);
    addBrick(520, 20);
    addBrick(560, 0);
    addBrick(600, 20);
    addBrick(640, 0);
    addBrick(680, 20);
    addBrick(720, 0);
    addBrick(760, 20);

    addBrick(0,   40);
    addBrick(40,  60);
    addBrick(80,  40);
    addBrick(120, 60);
    addBrick(160, 40);
    addBrick(200, 60);
    addBrick(240, 40);
    addBrick(280, 60);
    addBrick(320, 40);
    addBrick(360, 60);
    addBrick(400, 40);
    addBrick(440, 60);
    addBrick(480, 40);
    addBrick(520, 60);
    addBrick(560, 40);
    addBrick(600, 60);
    addBrick(640, 40);
    addBrick(680, 60);
    addBrick(720, 40);
    addBrick(760, 60);

    addBrick(0,   80);
    addBrick(40,  100);
    addBrick(80,  80);
    addBrick(120, 100);
    addBrick(160, 80);
    addBrick(200, 100);
    addBrick(240, 80);
    addBrick(280, 100);
    addBrick(320, 80);
    addBrick(360, 100);
    addBrick(400, 80);
    addBrick(440, 100);
    addBrick(480, 80);
    addBrick(520, 100);
    addBrick(560, 80);
    addBrick(600, 100);
    addBrick(640, 80);
    addBrick(680, 100);
    addBrick(720, 80);
    addBrick(760, 100);

    addBrick(0,   120);
    addBrick(40,  140);
    addBrick(80,  120);
    addBrick(120, 140);
    addBrick(160, 120);
    addBrick(200, 140);
    addBrick(240, 120);
    addBrick(280, 140);
    addBrick(320, 120);
    addBrick(360, 140);
    addBrick(400, 120);
    addBrick(440, 140);
    addBrick(480, 120);
    addBrick(520, 140);
    addBrick(560, 120);
    addBrick(600, 140);
    addBrick(640, 120);
    addBrick(680, 140);
    addBrick(720, 120);
    addBrick(760, 140);

    addBrick(0,   160);
    addBrick(40,  180);
    addBrick(80,  160);
    addBrick(120, 180);
    addBrick(160, 160);
    addBrick(200, 180);
    addBrick(240, 160);
    addBrick(280, 180);
    addBrick(320, 160);
    addBrick(360, 180);
    addBrick(400, 160);
    addBrick(440, 180);
    addBrick(480, 160);
    addBrick(520, 180);
    addBrick(560, 160);
    addBrick(600, 180);
    addBrick(640, 160);
    addBrick(680, 180);
    addBrick(720, 160);
    addBrick(760, 180);

    addBrick(0,   200);
    addBrick(40,  220);
    addBrick(80,  200);
    addBrick(120, 220);
    addBrick(160, 200);
    addBrick(200, 220);
    addBrick(240, 200);
    addBrick(280, 220);
    addBrick(320, 200);
    addBrick(360, 220);
    addBrick(400, 200);
    addBrick(440, 220);
    addBrick(480, 200);
    addBrick(520, 220);
    addBrick(560, 200);
    addBrick(600, 220);
    addBrick(640, 200);
    addBrick(680, 220);
    addBrick(720, 200);
    addBrick(760, 220);

    addBrick(0,   240);
    addBrick(40,  260);
    addBrick(80,  240);
    addBrick(120, 260);
    addBrick(160, 240);
    addBrick(200, 260);
    addBrick(240, 240);
    addBrick(280, 260);
    addBrick(320, 240);
    addBrick(360, 260);
    addBrick(400, 240);
    addBrick(440, 260);
    addBrick(480, 240);
    addBrick(520, 260);
    addBrick(560, 240);
    addBrick(600, 260);
    addBrick(640, 240);
    addBrick(680, 260);
    addBrick(720, 240);
    addBrick(760, 260);

    addBrick(0,   280);
    addBrick(40,  300);
    addBrick(80,  280);
    addBrick(120, 300);
    addBrick(160, 280);
    addBrick(200, 300);
    addBrick(240, 280);
    addBrick(280, 300);
    addBrick(320, 280);
    addBrick(360, 300);
    addBrick(400, 280);
    addBrick(440, 300);
    addBrick(480, 280);
    addBrick(520, 300);
    addBrick(560, 280);
    addBrick(600, 300);
    addBrick(640, 280);
    addBrick(680, 300);
    addBrick(720, 280);
    addBrick(760, 300);

    return 160;

}


int scene::level3(){
    QGraphicsPixmapItem *bon;
    void (scene::*pBonusFunction)();

    bon = addPixmap(QPixmap("br.png"));
    bon->setPos(100,100);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::bigRacket;
    mapBonus.insert(bon, pBonusFunction);

    bon = addPixmap(QPixmap("mb.png"));
    bon->setPos(360, 160);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::manyBalls;
    mapBonus.insert(bon, pBonusFunction);

    bon = addPixmap(QPixmap("+1.png"));
    bon->setPos(380, 220);
    bon->setData(0,"bonus");
    pBonusFunction = &scene::healthPlus;
    mapBonus.insert(bon, pBonusFunction);

    addBrick(0,   0);
    addBrick(40,  0);
    addBrick(80,  0);
    addBrick(120, 0);
    addBrick(160, 0);
    addBrick(200, 0);
    addBrick(240, 0);
    addBrick(280, 0);
    addBrick(320, 0);
    addBrick(360, 0);
    addBrick(400, 0);
    addBrick(440, 0);
    addBrick(480, 0);
    addBrick(520, 0);
    addBrick(560, 0);
    addBrick(600, 0);
    addBrick(640, 0);
    addBrick(680, 0);
    addBrick(720, 0);
    addBrick(760, 0);

    addBrick(20,  20);
    addBrick(60,  20);
    addBrick(100, 20);
    addBrick(140, 20);
    addBrick(180, 20);
    addBrick(220, 20);
    addBrick(260, 20);
    addBrick(300, 20);
    addBrick(340, 20);
    addBrick(380, 20);
    addBrick(420, 20);
    addBrick(460, 20);
    addBrick(500, 20);
    addBrick(540, 20);
    addBrick(580, 20);
    addBrick(620, 20);
    addBrick(660, 20);
    addBrick(700, 20);
    addBrick(740, 20);

    addBrick(40,  40);
    addBrick(80,  40);
    addBrick(120, 40);
    addBrick(160, 40);
    addBrick(200, 40);
    addBrick(240, 40);
    addBrick(280, 40);
    addBrick(320, 40);
    addBrick(360, 40);
    addBrick(400, 40);
    addBrick(440, 40);
    addBrick(480, 40);
    addBrick(520, 40);
    addBrick(560, 40);
    addBrick(600, 40);
    addBrick(640, 40);
    addBrick(680, 40);
    addBrick(720, 40);

    addBrick(60,  60);
    addBrick(100, 60);
    addBrick(140, 60);
    addBrick(180, 60);
    addBrick(220, 60);
    addBrick(260, 60);
    addBrick(300, 60);
    addBrick(340, 60);
    addBrick(380, 60);
    addBrick(420, 60);
    addBrick(460, 60);
    addBrick(500, 60);
    addBrick(540, 60);
    addBrick(580, 60);
    addBrick(620, 60);
    addBrick(660, 60);
    addBrick(700, 60);

    addBrick(80,  80);
    addBrick(120, 80);
    addBrick(160, 80);
    addBrick(200, 80);
    addBrick(240, 80);
    addBrick(280, 80);
    addBrick(320, 80);
    addBrick(360, 80);
    addBrick(400, 80);
    addBrick(440, 80);
    addBrick(480, 80);
    addBrick(520, 80);
    addBrick(560, 80);
    addBrick(600, 80);
    addBrick(640, 80);
    addBrick(680, 80);

    addBrick(100, 100);
    addBrick(140, 100);
    addBrick(180, 100);
    addBrick(220, 100);
    addBrick(260, 100);
    addBrick(300, 100);
    addBrick(340, 100);
    addBrick(380, 100);
    addBrick(420, 100);
    addBrick(460, 100);
    addBrick(500, 100);
    addBrick(540, 100);
    addBrick(580, 100);
    addBrick(620, 100);
    addBrick(660, 100);

    addBrick(120, 120);
    addBrick(160, 120);
    addBrick(200, 120);
    addBrick(240, 120);
    addBrick(280, 120);
    addBrick(320, 120);
    addBrick(360, 120);
    addBrick(400, 120);
    addBrick(440, 120);
    addBrick(480, 120);
    addBrick(520, 120);
    addBrick(560, 120);
    addBrick(600, 120);
    addBrick(640, 120);

    addBrick(140, 140);
    addBrick(180, 140);
    addBrick(220, 140);
    addBrick(260, 140);
    addBrick(300, 140);
    addBrick(340, 140);
    addBrick(380, 140);
    addBrick(420, 140);
    addBrick(460, 140);
    addBrick(500, 140);
    addBrick(540, 140);
    addBrick(580, 140);
    addBrick(620, 140);

    addBrick(160, 160);
    addBrick(200, 160);
    addBrick(240, 160);
    addBrick(280, 160);
    addBrick(320, 160);
    addBrick(360, 160);
    addBrick(400, 160);
    addBrick(440, 160);
    addBrick(480, 160);
    addBrick(520, 160);
    addBrick(560, 160);
    addBrick(600, 160);

    addBrick(180, 180);
    addBrick(220, 180);
    addBrick(260, 180);
    addBrick(300, 180);
    addBrick(340, 180);
    addBrick(380, 180);
    addBrick(420, 180);
    addBrick(460, 180);
    addBrick(500, 180);
    addBrick(540, 180);
    addBrick(580, 180);

    addBrick(200, 200);
    addBrick(240, 200);
    addBrick(280, 200);
    addBrick(320, 200);
    addBrick(360, 200);
    addBrick(400, 200);
    addBrick(440, 200);
    addBrick(480, 200);
    addBrick(520, 200);
    addBrick(560, 200);

    addBrick(220, 220);
    addBrick(260, 220);
    addBrick(300, 220);
    addBrick(340, 220);
    addBrick(380, 220);
    addBrick(420, 220);
    addBrick(460, 220);
    addBrick(500, 220);
    addBrick(540, 220);

    addBrick(240, 240);
    addBrick(280, 240);
    addBrick(320, 240);
    addBrick(360, 240);
    addBrick(400, 240);
    addBrick(440, 240);
    addBrick(480, 240);
    addBrick(520, 240);

    addBrick(260, 260);
    addBrick(300, 260);
    addBrick(340, 260);
    addBrick(380, 260);
    addBrick(420, 260);
    addBrick(460, 260);
    addBrick(500, 260);

    return 189;
}
