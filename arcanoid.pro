#-------------------------------------------------
#
# Project created by QtCreator 2012-06-13T20:11:34
#
#-------------------------------------------------

QT += core gui
QT += openglwidgets
greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

TARGET = arcanoid
TEMPLATE = app


SOURCES += main.cpp\
        arcanoid.cpp \
    scene.cpp \
    ball.cpp

HEADERS  += arcanoid.h \
    scene.h \
    ball.h

FORMS    += arcanoid.ui

RESOURCES += \
    resource.qrc
