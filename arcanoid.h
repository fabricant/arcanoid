#ifndef ARCANOID_H
#define ARCANOID_H

#include <QMainWindow>

namespace Ui {
class arcanoid;
}

class arcanoid : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit arcanoid(QWidget *parent = 0);
    void resizeEvent(QResizeEvent *);
    ~arcanoid();
    

private:
    Ui::arcanoid *ui;
};

#endif // ARCANOID_H
