#ifndef SCENE_H
#define SCENE_H

#include<QWidget>
#include <QGraphicsScene>
//#include <QGraphicsRectItem>
//#include <QGraphicsSceneMouseEvent>
#include <QTimer>
//#include <QGraphicsItemAnimation>
#include <QMap>
#include "ball.h"

class scene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit scene(QObject *parent = 0);
signals:
    void healthSignal(QString health);

private:
    QGraphicsPixmapItem *racket;
    QTimer timer;
    QMap <QGraphicsPixmapItem*,void(scene::*)()> mapBonus;
    QList<QGraphicsPixmapItem*>listBonus;
    QList<QGraphicsPixmapItem*>round;
    QList<Ball*> ballList;
    int brickCount;

    int   level1();
    int   level2();
    int   level3();
    short level;
    void  ballMoveX(Ball *ball);
    void  ballMoveY(float &rx,  Ball *ball);
    void  racketMove(float &rx);
//    bool  inRacket(QGraphicsPixmapItem *item);
    void  manyBalls();
    void  bigRacket();
    void  healthPlus();
    float getAngle(float &rx);
    void  getAngle(float &rx , float &angle);
    short health;
    QGraphicsPixmapItem* addBrick(int x, int y);
    Ball* addBall(int x, int y, short verticalDirection, float angle, bool move = true);

    
public slots:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    void keyPressEvent(QKeyEvent *event);

    void updateScene();

    void normalRacket();

    void  newGame();


};

#endif // SCENE_H
